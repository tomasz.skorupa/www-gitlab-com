---
layout: markdown_page
title: "Category Strategy - Memory"
description: "The Memory team works on making GitLab perform well at any scale."
canonical_path: "/direction/memory/"
---

- TOC
{:toc}

## 🛠️ Memory group

**Last updated**: 2022-06-15

### Introduction and how you can help

- [Overall Strategy](/direction/enablement)
- [Roadmap for the Memory Group](https://gitlab.com/groups/gitlab-org/-/roadmap?state=opened&sort=end_date_asc&label_name%5B%5D=group%3A%3Amemory&label_name%5B%5D=Roadmap)
- [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory)

Please reach out to Yannis Roussos, Product Manager for the Memory group ([Email](mailto:iroussos@gitlab.com)) if you'd like to provide feedback or ask any questions related to this product category.

This strategy is a work in progress, and everyone can contribute. Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Amemory) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.

### Overview

GitLab should be as lighweight as possible and perform at any scale. GitLab is used on Raspberry Pis for only a few users, large organizations with thousands of users and up to the scale of GitLab.com. The Memory team is responsible for application-wide concerns that impact the availability, reliability and performance of GitLab at any scale. The Memory team analyzes the behavior of GitLab, for example with regards to memory consumption, recognizes bottlenecks, proposes changes to the application and works on overall improvements to ensure that GitLab works well for self-managed customers and our SaaS offering alike.

The Memory team regularly collaborates with teams across GitLab to support high-impact initiatives that reduce memory consumption, change the overall design of the application or helps other teams with the design of new features. 

The Memory team focuses on the MVC to iterate as fast as possible, strict prioritization on only the highest-impact areas and clean handover to other teams. Learn more about [how the team works](/handbook/engineering/development/enablement/memory/).

### Differences to GitLab's Scalability team

The [Scalability team](/handbook/engineering/infrastructure/team/scalability/) is primarily concerned with the performance and availability of GitLab.com and focuses on helping other teams become better at scalability. The Memory Group is concerned with application-wide concerns at any scale, ranging from very small installations to GitLab.com.

### Where we are headed

The Memory team's short term goals are to continuously identify the main bottlenecks that affect GitLab's performance and availability and address those - either as a stand-alone project or in collaboration with the teams that are directly responsible for that area.

In medium to long term the Memory team wants to provide better tooling, metrics and visibility to development teams that allow them to make better data-driven decisions when developing new features or iterating on existing ones. Using this approach, we want to shift performance, memory and application issues further left so that they can be addressed *before* being implemented. This will enable GitLab itself to become more proactive during development. Ultimately, these features should be integrated into GitLab itself so that our users can benefit from the same tooling.

### What's Next & Why

#### [Improve efficiency and maintainability of application metrics exporters](https://gitlab.com/groups/gitlab-org/-/epics/7397)

We are revisiting our approach for serving application metrics into Prometheus.

We currently use two competing approaches to serve application metrics into Prometheus:
- [In-app exporters](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/metrics/exporter) that are part of the Rails codebase

- [gitlab-exporter](https://gitlab.com/gitlab-org/gitlab-exporter/), an independently deployed collector and server

Our plan is to implement a new integrated way of exporting metrics that:
- provides a single application exporter system, which subsumes both gitlab-exporter and app-internal exporters

- runs outside of the Rails monolith

- performs efficiently in face of large data volumes (tens to hundreds of thousands of samples per scrape)

We have completed the initial necessary steps of [exporting the Sidekiq metrics from a separate process](https://gitlab.com/groups/gitlab-org/-/epics/6409)
and [moving the metrics server out of Puma](https://gitlab.com/groups/gitlab-org/-/epics/7304) and we are now working on
[adding a new metrics exporter for GitLab application metrics](https://gitlab.com/groups/gitlab-org/-/epics/7396) which is written in Golang.

#### [Investigate Puma long-term memory use](https://gitlab.com/groups/gitlab-org/-/epics/8105)

We have observed that Puma can see climbing memory use in production over a period of days, especially during weekends, where there are no deploys that would reset these gauges.
Meanwhile, this is also affecting self managed instances; we are getting an increased number of customer reports complaining that the Puma memory killer frequently kills Puma processes, which causes throughput issues.

We plan to investigate further and try to figure out the reason for any potential runaway memory issues, be it just Ruby heap fragmentation or a memory leak.

#### [Update supported Ruby version to 3.0](https://gitlab.com/groups/gitlab-org/-/epics/5149)

The End Of Life for Ruby 2.7, which we currently use in GitLab, is expected to be March 2023 based on life expectancy of previous major releases, so we want to complete this upgrade sooner than later.
Ruby 3.0 has also a few additional, even though minor, improvements that could be beneficial regarding performance, concurrency and static analysis.

In 15.0, we have achieved our primary goal of [achieving a green Ruby 3 GitLab build](https://gitlab.com/groups/gitlab-org/-/epics/7887).

In 15.1, we plan to remove any other remaining blockers and work towards adding Ruby 3 builds for various components and GitLab libraries.
We don't expect to switch our [Ruby 2 and Ruby 3 testing pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/340298) yet,
but we'll be monitoring and prepare for when we are ready to build Ruby 3 by default.

#### [Create custom SLIs for Global Search](https://gitlab.com/groups/gitlab-org/-/epics/7892)

The existing SLI for global search is defined over a single end point (`/search`). That means that there is no visibility for the performance
or other issues encountered on the different types of searches (basic / advanced) or scopes (e.g. code, issues, merge requests, etc).

Those types of searches and scopes vary in execution approach (through Elasticsearch, PostgreSQL or even Gitaly) and characteristics, which makes the aggregate performance and error metrics not that useful.

We want to support the Global Search Group on setting up the custom SLIs for this overloaded end point.
We will follow the [results of the discussion with the Scalability team](https://gitlab.com/groups/gitlab-org/search-team/-/epics/2#note_691510478) and start by introducing the `global_search_apdex` and `global_search_success` custom SLIs.
We will also evaluate how can we facilitate the introduction of the `global_search_indexing_apdex` custom SLI.

### Metrics

- [Section performance indicator - Memory consumed](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/enablement-section/#enablementmemory---group-ppi---memory-consumed) (internal only)
